/*

[x] open err.ee
[x] pealkiri on "uudised | ERR"
[x] vota screenshot
[x] page contains Eesti link
[] klikkides Eesti lingi peale avaneb uus sait mille peakiri on "Eesti | ERR"
[] vota screenshot

*/

const config = require('../../nightwatch.conf.js');

module.exports = {
  'Err Uudised': function (browser) {
    browser
      .url('https://www.err.ee/')
      .windowSize('current', 1500, 1500)
      .waitForElementVisible('body')
      .assert.title('uudised | ERR')
      .saveScreenshot(`${config.imgpath(browser)}err-esileht.png`)
      .assert.elementPresent('.siteDropdown')
      .useXpath()
      .click('//*[@class="siteDropdown"]//a[@href="/k/eesti"]')
      .pause(500)
      .useCss()
      .waitForElementVisible('body')
      .assert.title('Eesti | ERR')
      .saveScreenshot(`${config.imgpath(browser)}err-eesti.png`)
      .assert.cssProperty('.category-name', 'text-transform', 'uppercase')
      .assert.urlEquals('https://www.err.ee/k/eesti')
      .assert.attributeEquals('#searchForm', 'method', 'get')
      .assert.attributeContains('.global-search', 'placeholder', 'Otsi')
      .assert.containsText('.cookieOverlayText', 'küpsiseid')
      .assert.cssClassPresent('#main', 'test fp-container')
      .end();
  },
};
